const Model = require("../model/Trade");

class TradeController {
  static async find(req, res) {
    try {
      await Model.find()
        .then(data => res.status(200).json({ data }))
        .catch(err => console.log("Error fetching Traders", err));
    } catch (error) {
      console.log(error);
    }
  }

  static async store(req, res) {
    try {
      await Model.store(req.body)
        .then(() =>
          res.status(201).json({ message: "Trade created successfully" })
        )
        .catch(err => console.log("Error created Trade", err));
    } catch (error) {
      console.log(error);
    }
  }

  static async filterByDate(req, res) {
    let { startDate, endDate } = req.body;

    if (!startDate) return;
    if (!endDate) endDate = startDate;

    try {
      await Model.filterByDate(startDate, endDate)
        .then(data => res.status(200).json({ data }))
        .catch(err => console.log("Error filter trade by date", err));
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = TradeController;
