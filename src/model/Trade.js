const db = require("../config/database");

module.exports = {
  find: () => {
    return new Promise((resolve, reject) => {
      db.execute("SELECT * FROM Traders", [], (err, data) =>
        err ? reject(err) : resolve(data)
      );
    });
  },

  store: data => {
    if (!data) return;

    const {
      active,
      input,
      contracts,
      operation,
      strategy,
      output,
      points,
      created
    } = data;

    return new Promise((resolve, reject) => {
      db.execute(
        "INSERT INTO Traders (Active, Input, Contracts, Operation, Strategy, Output, Points, CreatedAt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        [
          active,
          input,
          contracts,
          operation,
          strategy,
          output,
          points,
          created
        ],
        (err, data) => (err ? reject(err) : resolve(data))
      );
    });
  },

  filterByDate: (startDate, endDate) => {
    return new Promise((resolve, reject) => {
      db.execute(
        "SELECT * FROM Traders WHERE CreatedAt BETWEEN ? AND ?",
        [startDate, endDate],
        (err, data) => (err ? reject(err) : resolve(data))
      );
    });
  }
};
