const express = require("express");
const routes = express.Router();

const TradeController = require("./controllers/TradeController");

routes.get("/", TradeController.find);
routes.post("/trade", TradeController.store);
routes.post("/filter", TradeController.filterByDate);

module.exports = routes;
