const express = require("express");
const app = express();
const cors = require("cors");

const port = process.env.PORT || 3333;
const url = process.env.URL || "http://localhost";

app.use(express.json());
app.use(cors());
app.use(require("./routes"));

app.listen(port, err => {
  if (err) {
    console.log("Error running server Traders", err);
    return;
  }

  console.log(`Server running at ${url}:${port}`);
});
